Place for everything Web Programming using Python.  

Lessons
-------

* [01 - Web Programming](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/learn-web-programming/raw/master/notebooks/01%20-%20Web%20Programming.ipynb)
* [02 - Web Programming](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/learn-web-programming/raw/master/notebooks/02%20-%20Web%20Programming.ipynb)
* [03 - Web Programming](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/learn-web-programming/raw/master/notebooks/03%20-%20Web%20Programming.ipynb)
* [04 - Web Programming](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/learn-web-programming/raw/master/notebooks/04%20-%20Web%20Programming.ipynb)
* [05 - Web Programming](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/learn-web-programming/raw/master/notebooks/05%20-%20Web%20Programming.ipynb)